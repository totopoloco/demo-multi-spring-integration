package at.mavila.multisi.integration.configuration;

import at.mavila.multisi.app.configuration.Module1Configuration;
import at.mavila.multisi.app.configuration.MyPojo;
import jakarta.annotation.PostConstruct;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.context.Lifecycle;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.PollerSpec;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.dsl.context.IntegrationFlowContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class IntegrationFlowDynamicRegistrar {

  private final IntegrationFlowContext integrationFlowContext;
  private final Module1Configuration module1Configuration;

  public IntegrationFlowDynamicRegistrar(
      final IntegrationFlowContext integrationFlowContext,
      final Module1Configuration module1Configuration) {
    this.integrationFlowContext = integrationFlowContext;
    this.module1Configuration = module1Configuration;
  }

  @PostConstruct
  public void createIntegrationFlows() {
    module1Configuration.getStation().forEach((key, value) -> {
      IntegrationFlowContext.IntegrationFlowRegistration registered = this.integrationFlowContext.registration(
          createIntegrationFlow(value, key)
      ).register();
      startIntegrationFlow(registered.getId());
    });


  }

  private void startIntegrationFlow(String flowId) {
    IntegrationFlow flow = integrationFlowContext.getRegistrationById(flowId).getIntegrationFlow();
    if (flow instanceof Lifecycle lifecycle) {
      log.info("Starting integration flow at: <{}>", new Date());
      lifecycle.start();
    }
  }

  private IntegrationFlow createIntegrationFlow(MyPojo myPojo, final String identifier) {

    return IntegrationFlow
        .from(
            () -> getMessage(myPojo, identifier),
            e -> e.poller(pollerFactory -> getPollerSpec(myPojo)))
        .enrichHeaders(headerEnricherSpec ->
            headerEnricherSpec
                .headerFunction("length", m -> "Length is: " + ((String) m.getPayload()).length()))
        .handle(message ->
        {
          Object payload = message.getPayload();
          log.info(payload.toString());
          log.info(new Date().toString());
          MessageHeaders headers = message.getHeaders();
          log.info((String) headers.get("length"));
          //Cause a random exception
          Random randI = new Random();
          int myRandInt = randI.nextInt(3);

          if (myRandInt == 2) {
            // throw new IllegalStateException("An exception occurred");
          }

        })
        .get();
  }

  private PollerSpec getPollerSpec(MyPojo myPojo) {
    return Pollers
        .trigger(new PeriodicTrigger(Duration.of(myPojo.getDelay(), ChronoUnit.SECONDS)))
        .taskExecutor(getTaskExecutor())
        .errorHandler(t -> {
          Throwable rootCause = ExceptionUtils.getRootCause(t);
          log.warn("Failed to trigger: <{}>", rootCause);
        });
  }

  private TaskExecutor getTaskExecutor() {
    final var poolSize = this.module1Configuration.getPoolSize();
    log.info("...... creating Demo ThreadPool of size {}.", poolSize);
    final var executor = new ThreadPoolTaskExecutor();
    executor.setThreadNamePrefix("Demo_pool_");
    executor.setCorePoolSize(poolSize);
    executor.initialize();
    return executor;
  }

  private static Message<Object> getMessage(MyPojo key, String identifier) {
    Random random = new Random();

    int min = 1;
    int max = 10;

    int value = random.nextInt(max + min) + min;
    return MessageBuilder.<Object>withPayload(
            RandomStringUtils.randomAscii(value) + " - " + identifier + " - " + key.getKey2() + "-" + key.getKey3())
        .build();
  }


}
