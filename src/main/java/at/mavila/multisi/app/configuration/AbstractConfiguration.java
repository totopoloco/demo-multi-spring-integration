package at.mavila.multisi.app.configuration;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@ToString
@Slf4j
public abstract class AbstractConfiguration {
  protected Map<String, MyPojo> station;
  protected int poolSize;
}