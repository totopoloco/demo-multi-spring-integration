package at.mavila.multisi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.IntegrationComponentScan;

@SpringBootApplication
@IntegrationComponentScan
public class MultisiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultisiApplication.class, args);
	}

}
